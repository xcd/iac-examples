#!/usr/bin/env bash
set -o allexport
build_conf="build-win.conf"

function help {
    printf "\n"
    echo "$0 (win-server|debug-win-server)"
    echo
    echo "win-server     - Build an Windows Server 2019 VM template"
    echo "debug-win-server - Debug Mode: Build an Windows Server 2019 VM template"
    echo
    echo "Provide Proxmox token via ENV variable:"
    printf "\n"
    exit 0
}

function var_is_not_present {
    echo "ERROR: Variable '$1' is required to be set. Please edit '${build_conf}' and set."
    exit 1
}

[[ -f $build_conf ]] || { echo "User variables file '$build_conf' not found."; exit 1; }
source $build_conf

[[ -z "$vm_default_user" ]] && var_is_not_present "vm_default_user"
[[ -z "$default_vm_id" ]] && var_is_not_present "default_vm_id"
[[ -z "$windows_iso_url" ]] && var_is_not_present "windows_iso_url"
[[ -z "$virtio_drivers_iso_url" ]] && var_is_not_present "virtio_drivers_iso_url"
[[ -z "$iso_directory" ]] && var_is_not_present "iso_directory"

## check if build arg is passed to script
target=${1:-}
[[ "${1}" == "win-server" ]] || [[ "${1}" == "debug-win-server" ]] || help

[[ $(packer --version)  ]] || { echo "Please install 'Packer'"; exit 1; }
[[ $(ansible --version)  ]] || { echo "Please install 'Ansible'"; exit 1; }
[[ $(j2 --version)  ]] || { echo "Please install 'j2cli'"; exit 1; }

[[ -z "$proxmox_token" ]] && printf "\n" && read -s -p "Proxmox token: " proxmox_token && printf "\n"
printf "\n"

if [[ -z "$ssh_password" ]]; then
    export ssh_password=$vm_default_user #doesn't matter because of inject_user_password
fi

[[ -z "$proxmox_token" ]] && echo "The Proxmox token is required." && exit 1

printf "\n==> Downloading Windows ISO\n\n"
windows_iso_filename=$(basename $windows_iso_url)
wget -P $iso_directory -N $windows_iso_url  # only re-download when newer on the server

printf "\n==> Downloading VirtIO drivers ISO\n\n"
virtio_drivers_iso_filename=$(basename $virtio_drivers_iso_url)
wget -P $iso_directory -N $virtio_drivers_iso_url

## create floppy Autounattend.iso
genisoimage -J -l -R -V "Autounattend" -iso-level 4 -o /mnt/storage/template/iso/Autounattend.iso $PWD/floppy

export vm_default_user=$vm_default_user
export ssh_password=$ssh_password

case $target in
    win-server)
        printf "\n==> Build and create an Windows Server 2019 template.\n\n"
        packer build -var iso_filename=$windows_iso_filename -var vm_id=$default_vm_id -var proxmox_token=$proxmox_token -var ssh_password=$ssh_password -var ssh_username=$vm_default_user win2019.json
        ;;
    debug-win-server)
        printf "\n==> DEBUG: Build and create an Windows Server 2019 template.\n\n"
        PACKER_LOG=1 packer build -debug -on-error=ask -var so_filename=$windows_iso_filename -var vm_id=$default_vm_id -var proxmox_token=$proxmox_token -var ssh_password=$ssh_password -var ssh_username=$vm_default_user win2019.json
        ;;
    *)
        help
        ;;
esac

printf "==> " && rm -v /mnt/storage/template/iso/Autounattend.iso