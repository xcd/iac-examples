cinst -y cloudbaseinit

$confFile = 'cloudbase-init.conf'
$unattendConfFile = 'cloudbase-init-unattend.conf'
$unattendFile = 'Unattend.xml'

$confPath = 'C:\Program Files\Cloudbase Solutions\Cloudbase-Init\conf\'
rm -Path ($confPath + $confFile) -force
rm -Path ($confPath + $unattendConfFile) -force 
rm -Path ($confPath + $unattendFile) -force

$confContent = @"
[DEFAULT]
username=Admin
groups=Administrators
inject_user_password=true

config_drive_raw_hhd=true
config_drive_cdrom=true
config_drive_vfat=true

bsdtar_path=C:\Program Files\Cloudbase Solutions\Cloudbase-Init\bin\bsdtar.exe
mtools_path=C:\Program Files\Cloudbase Solutions\Cloudbase-Init\bin\

verbose=true
debug=true
logdir=C:\Program Files\Cloudbase Solutions\Cloudbase-Init\log\
logfile=cloudbase-init.log
default_log_levels=comtypes=INFO,suds=INFO,iso8601=WARN,requests=WARN
logging_serial_port_settings=

mtu_use_dhcp_config=true
ntp_use_dhcp_config=true

local_scripts_path=C:\Program Files\Cloudbase Solutions\Cloudbase-Init\LocalScripts\

check_latest_version=false
allow_reboot=false
stop_service_on_exit=false

metadata_services=cloudbaseinit.metadata.services.configdrive.ConfigDriveService
plugins=cloudbaseinit.plugins.common.networkconfig.NetworkConfigPlugin,
    cloudbaseinit.plugins.common.userdata.UserDataPlugin,
    cloudbaseinit.plugins.common.sethostname.SetHostNamePlugin,
    cloudbaseinit.plugins.common.mtu.MTUPlugin,
    cloudbaseinit.plugins.windows.extendvolumes.ExtendVolumesPlugin
"@

$unattendContent = [xml] @"
<?xml version="1.0" encoding="utf-8"?>
<unattend xmlns="urn:schemas-microsoft-com:unattend">
  <settings pass="generalize">
    <component name="Microsoft-Windows-PnpSysprep" processorArchitecture="amd64" publicKeyToken="31bf3856ad364e35" language="neutral" versionScope="nonSxS" xmlns:wcm="http://schemas.microsoft.com/WMIConfig/2002/State" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      <PersistAllDeviceInstalls>true</PersistAllDeviceInstalls>
      <!--DoNotCleanUpNonPresentDevices>true</DoNotCleanUpNonPresentDevices-->
    </component>
    <!--component name="Microsoft-Windows-Security-SPP" processorArchitecture="amd64" publicKeyToken="31bf3856ad364e35" language="neutral" versionScope="nonSxS" xmlns:wcm="http://schemas.microsoft.com/WMIConfig/2002/State" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <SkipRearm>1</SkipRearm>
    </component-->
  </settings>
  <settings pass="oobeSystem">
    <component name="Microsoft-Windows-Shell-Setup" processorArchitecture="amd64" publicKeyToken="31bf3856ad364e35" language="neutral" versionScope="nonSxS" xmlns:wcm="http://schemas.microsoft.com/WMIConfig/2002/State">
      <OOBE>
        <HideEULAPage>true</HideEULAPage>
        <SkipMachineOOBE>true</SkipMachineOOBE>
        <SkipUserOOBE>true</SkipUserOOBE>
        <NetworkLocation>Work</NetworkLocation>
        <ProtectYourPC>3</ProtectYourPC>
      </OOBE>
    </component>
  </settings>
  <settings pass="specialize">
    <component name="Microsoft-Windows-Deployment" processorArchitecture="amd64" publicKeyToken="31bf3856ad364e35" language="neutral" versionScope="nonSxS" xmlns:wcm="http://schemas.microsoft.com/WMIConfig/2002/State" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      <RunSynchronous>
        <RunSynchronousCommand wcm:action="add">
          <Order>1</Order>
          <Description>Manual start cloudbase-init</Description>
          <Path>powershell -ExecutionPolicy Bypass -command "Start-Process sc.exe -ArgumentList 'start cloudbase-init' -wait"</Path>
          <WillReboot>Never</WillReboot>
        </RunSynchronousCommand>
        <!--RunSynchronousCommand wcm:action="add">
          <Order>2</Order>
          <Description>Re-enable auto start of cloudbase-init</Description>
          <Path>powershell -ExecutionPolicy Bypass -command "Start-Process sc.exe -ArgumentList 'config cloudbase-init start= auto' -wait"</Path>
          <WillReboot>Never</WillReboot>
        </RunSynchronousCommand-->
        <RunSynchronousCommand wcm:action="add">
          <Order>3</Order>
          <Path>cmd.exe /c ""C:\Program Files\Cloudbase Solutions\Cloudbase-Init\Python\Scripts\cloudbase-init.exe" --config-file "C:\Program Files\Cloudbase Solutions\Cloudbase-Init\conf\cloudbase-init.conf" &amp;&amp; exit 1 || exit 2"</Path>
          <WillReboot>OnRequest</WillReboot>
        </RunSynchronousCommand>
      </RunSynchronous>
    </component>
  </settings>
</unattend>
"@

New-Item -Path $confPath -Name $confFile -ItemType File -Force -Value $confContent
$unattendContent.save("$confPath\$unattendFile")

Start-Process sc.exe -ArgumentList "config cloudbase-init start= auto" -wait
