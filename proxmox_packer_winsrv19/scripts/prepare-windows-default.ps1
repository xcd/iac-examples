#workaround for Powershell 5.1 bug, related with ArgumentOutOfRangeException while pasting very long strings
powershell -ExecutionPolicy Bypass -Command "install-packageprovider -name nuget -minimumversion 2.8.5.201 -force"
powershell -ExecutionPolicy Bypass -Command "Install-Module -Name PowerShellGet -Force"
powershell -ExecutionPolicy Bypass -Command "Install-Module PSReadLine -force"
##################################################################################################

$LanguageList = Get-WinUserLanguageList
$LanguageList.Add("fr-FR")
Set-WinUserLanguageList $LanguageList -force

New-Item -Path "HKLM:\SOFTWARE\Policies\Microsoft" -Name "WindowsInkWorkspace" -Force
New-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\WindowsInkWorkspace" -Name "AllowWindowsInkWorkspace" -Value 0 -Force

$adminIEsecurityregpath = "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}"
$adminIEsecuritykey = "IsInstalled"
Set-ItemProperty -Path $adminIEsecurityregpath -Name $adminIEsecuritykey -Value 0

$features = "NET-Framework-45-Features", "NET-Framework-45-Core",
            "NET-Framework-45-ASPNET", "NET-WCF-Services45", 
            "NET-WCF-TCP-PortSharing45", "WoW64-Support"

foreach ($feature in $features) {
    if ((Get-WindowsFeature -name $feature | where installed) -ine $true) {Install-WindowsFeature -name $feature}
}

Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
cinst -y vcredist-all 7zip.install firefox notepadplusplus dotnetfx 

Install-WUUpdates -Updates $(Start-WUScan -SearchCriteria "Type='Software' AND IsInstalled=0") 
