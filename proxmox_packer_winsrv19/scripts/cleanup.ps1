#disable WinRM service and disable firewall rule
#winrm set winrm/config/service '@{AllowUnencrypted="false"}'
#winrm set winrm/config/service/auth '@{Basic="false"}'
#winrm set winrm/config/client/auth '@{Basic="false"}'
#sc.exe config winrm start= disabled
#netsh advfirewall firewall set rule group='Windows Remote Management' new enable=no

Get-ChildItem "C:\Windows\SoftwareDistribution\*" -Recurse -Force -Verbose -ErrorAction SilentlyContinue | Remove-Item -Force -Verbose -recurse -ErrorAction SilentlyContinue
Get-ChildItem "C:\Windows\Temp\*" -Recurse -Force -Verbose -ErrorAction SilentlyContinue | Remove-Item -Force -Verbose -recurse -ErrorAction SilentlyContinue
Get-ChildItem "C:\users\*\AppData\Local\Temp\*" -Recurse -Force -ErrorAction SilentlyContinue | Remove-Item -Force -Verbose -recurse -ErrorAction SilentlyContinue
Get-ChildItem "C:\users\*\AppData\Local\Microsoft\Windows\Temporary Internet Files\*" -Recurse -Force -Verbose -ErrorAction SilentlyContinue | Remove-Item -Force -recurse -ErrorAction SilentlyContinue

$sysprep = (gi -path "c:\Windows\System32\Sysprep\sysprep.exe").fullname
start-process -FilePath $sysprep -WorkingDirectory 'C:\Program Files\Cloudbase Solutions\Cloudbase-Init\conf'  -ArgumentList "/generalize /oobe /shutdown /unattend:Unattend.xml" -wait

#fork process to pass exit code 0 to packer 
#start-process powershell.exe -ArgumentList '-NoProfile -Command "sleep 5; shutdown /s /f /t 5; ipconfig /release"'