terraform init \
    -reconfigure \
    -backend-config="address=https://gitlab.com/api/v4/projects/22016582/terraform/state/${CI_PROJECT_NAME}" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/22016582/terraform/state/${CI_PROJECT_NAME}/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/22016582/terraform/state/${CI_PROJECT_NAME}/lock" \
    -backend-config="username=$TF_VAR_username" \
    -backend-config="password=$TF_PASSWORD" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
