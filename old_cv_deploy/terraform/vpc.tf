resource "aws_vpc" "main" {
	cidr_block = "10.1.0.0/16"
	tags = {
		Name = "Dedicated VPC"
	}
}

resource "aws_subnet" "main" {
	vpc_id = aws_vpc.main.id
	cidr_block = "10.1.1.0/24"
	availability_zone = "eu-west-1a"
}

resource "aws_internet_gateway" "main" {
	vpc_id = aws_vpc.main.id
}


resource "aws_route_table" "default" {
	vpc_id = aws_vpc.main.id
	route {
	  cidr_block = "0.0.0.0/0"
	  gateway_id = aws_internet_gateway.main.id
	}
}

resource "aws_route_table_association" "main" {
	subnet_id = aws_subnet.main.id
	route_table_id = aws_route_table.default.id
}

resource "aws_network_acl" "allow_web" {
	vpc_id = aws_vpc.main.id
	egress {
	  protocol = "tcp"
	  rule_no = 100 
	  action = "allow"
	  cidr_block = "0.0.0.0/0"
	  from_port = 443
	  to_port = 443
	}

    egress {
	  protocol = "tcp"
	  rule_no = 110 
	  action = "allow"
	  cidr_block = "0.0.0.0/0"
	  from_port = 80
	  to_port = 80
	}
 
	ingress {
	  protocol = "tcp"
	  rule_no = 200
	  action = "allow" 
	  cidr_block = "0.0.0.0/0"
	  from_port = 80
	  to_port = 80
	}

    ingress {
	  protocol = "tcp"
	  rule_no = 210
	  action = "allow" 
	  cidr_block = "0.0.0.0/0"
	  from_port = 443
	  to_port = 443
	}
}

resource "aws_security_group" "web" {
	name = "80/443"
	description = "Allows web" 
	vpc_id = aws_vpc.main.id

	ingress {
	  from_port = 443
	  to_port = 443
	  protocol = "tcp"
	  cidr_blocks = ["0.0.0.0/0"]
	}

	ingress {
	  from_port = 80
	  to_port = 80
	  protocol = "tcp"
	  cidr_blocks = ["0.0.0.0/0"]
	}

	ingress {
	  from_port = 22
	  to_port = 22
	  protocol = "tcp"
	  cidr_blocks = ["0.0.0.0/0"]
	}

	egress { 
	  from_port = 443
	  to_port = 443
	  protocol = "tcp"
	  cidr_blocks = ["0.0.0.0/0"]
	}

	egress { 
	  from_port = 80
	  to_port = 80
	  protocol = "tcp"
	  cidr_blocks = ["0.0.0.0/0"]
	}
}