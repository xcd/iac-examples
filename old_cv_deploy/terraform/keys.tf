resource "tls_private_key" "host-ecdsa" {
  algorithm = "ECDSA"
}

resource "tls_private_key" "host-rsa" {
  algorithm = "RSA"
  rsa_bits = 4096
}

resource "aws_key_pair" "generated_key" {
	key_name = var.key_name
	public_key = tls_private_key.host-rsa.public_key_openssh 
}

variable "key_name" {
	description = "Key name to use for the instance" 
	type = string 
	default = "key"
}
