variable "gitlabpwd" {
  type = string
}

variable "username" {
  type = string
}

variable "remote_address" {
  type = string
  default = "https://gitlab.com/api/v4/projects/22016582/terraform/state/cv-deploy"
}

 variable "access_key" {
  type = string
}

variable "secret_key" {
  type = string
}
