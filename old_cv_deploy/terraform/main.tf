terraform {
  backend "http" {
    address = var.remote_address
    username = var.username
    password = var.gitlabpwd
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}


provider "aws" {
  region = "eu-west-1"
  profile = "default"
  access_key = var.access_key
  secret_key = var.secret_key
}

data "aws_ami" "ubuntu" {
	most_recent = true
	filter {
	  name = "name" 
	  values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
	}
	filter { 
	  name = "virtualization-type"
	  values = ["hvm"]
	}
	owners = ["099720109477"]
}


resource "aws_instance" "webserver" {
  count                       = 1
  ami                         = data.aws_ami.ubuntu.id
  availability_zone           = "eu-west-1a"
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.generated_key.key_name
  vpc_security_group_ids      = [aws_security_group.web.id]
  subnet_id                   = aws_subnet.main.id
  associate_public_ip_address = true
  user_data = <<EOF
#!/bin/bash
set -e 
umask 077
echo '${tls_private_key.host-ecdsa.private_key_pem}' >/etc/ssh/ssh_host_ecdsa_key
echo '${tls_private_key.host-rsa.private_key_pem}' >/etc/ssh/ssh_host_rsa_key
sed -i -e 's/#HostKey \/etc\/ssh\/ssh_host_rsa_key/HostKey \/etc\/ssh\/ssh_host_rsa_key/' /etc/ssh/sshd_config
sed -i -e 's/#HostKey \/etc\/ssh\/ssh_host_ecdsa_key/HostKey \/etc\/ssh\/ssh_host_ecdsa_key/' /etc/ssh/sshd_config
rm /etc/ssh/ssh_host_dsa_key
rm /etc/ssh/ssh_host_ed25519_key
sed -i -e 's/#HostKey \/etc\/ssh\/ssh_host_dsa_key//' /etc/ssh/sshd_config
sed -i -e 's/#HostKey \/etc\/ssh\/ssh_host_ed25519_key//' /etc/ssh/sshd_config
EOF


  tags = {
    Name = "tf nginx cv"
  }

  connection {
    type = "ssh"
    port = 22
    agent = false
    private_key = tls_private_key.host-rsa.private_key_pem
    host = self.public_ip
    host_key = tls_private_key.host-rsa.public_key_openssh
    user = "ubuntu"
    timeout = "1m"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common",
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",
      "sudo add-apt-repository \"deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\" ",
      "sudo apt-get update",
      "sudo apt-get install -y docker-ce docker-ce-cli containerd.io",
      "sudo systemctl start docker",
      "echo ${var.gitlabpwd} | sudo docker login --username xcd --password-stdin registry.gitlab.com",
      "sudo docker run -p 80:80 -p 443:443 -d --restart=unless-stopped registry.gitlab.com/xcd/cv:latest"
    ]
   }
}


output "instance_ips" {
  value = [aws_instance.webserver[0].public_ip]
}
