#!/bin/bash
certbot-auto --nginx --non-interactive --agree-tos --register-unsafely-without-email -d www.web.site -d web.site && nginx -s reload && nginx -g "daemon off;"