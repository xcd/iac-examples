$inboundPorts = 22, 80, 443, 3389, 3391, 9182
$outboundPorts = 80, 443, 3389 

foreach ($port in $inboundPorts) {
    $par = @{
        DisplayName = "$port"
        LocalPort = $port
        Direction="Inbound"
        Protocol ="TCP" 
        Action = "Allow"
        Profile = "Any"
    }
    New-NetFirewallRule @par
}
foreach ($port in $outboundPorts) { 
    $par = @{
        DisplayName = "$port"
        LocalPort = $port
        Direction="Outbound"
        Protocol ="TCP" 
        Action = "Allow"
        Profile = "Any" 
    }
    New-NetFirewallRule @par 
}

