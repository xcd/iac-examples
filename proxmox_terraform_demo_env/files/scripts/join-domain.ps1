Set-DnsClientServerAddress -interfaceindex (get-netadapter | where InterfaceDescription -like "red hat*").ifIndex -serveraddress "$dc_ip" #set to static DC IP 
sleep 5

$u = "DOMAIN\administrator"
$pass = "${dc_admin_password}"
$pass = $pass | ConvertTo-SecureString -AsPlainText -Force
$cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $u, $pass
Add-Computer -DomainName boovatest.int -Credential $cred -Force

sleep 15 
#net user administrator /active:no
shutdown -r -t 0