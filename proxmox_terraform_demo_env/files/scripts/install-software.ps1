cinst -y notepadplusplus firefox 

#Consul
cinst -y nssm --force 
cinst -y consul --force --params '-config-dir="%PROGRAMDATA%\consul\config"'

$ip = (Test-Connection -ComputerName (hostname) -Count 1  | Select -ExpandProperty IPV4Address).IPAddressToString
$consulConfig = @"
{
    "server": false,
    "datacenter": "dc1",
    "data_dir": "c:/consul/data",
    "log_level": "INFO",
    "start_join": ["$consul_server_ip"],
    "bind_addr": "$ip"
}
"@
$consulConfig | out-file -FilePath "$env:PROGRAMDATA\consul\config\default.json" -Encoding Ascii -Force

$consulMetadata = @"
{
  "service":
  {"name": "windows_exporter",
   "tags": ["demo"],
   "port": 9182
  }
}
"@
$consulMetadata | out-file -FilePath "$env:PROGRAMDATA\consul\config\metadata.json" -Encoding Ascii -Force

#Prometheus
cinst -y prometheus-windows-exporter.install --params '"/EnabledCollectors:cpu,dns,iis,net,os,service,system,textfile,cs /ListenPort:9182"'
sc.exe config windows_exporter start=delayed-auto

#Wazuh 
cinst -y wazuh-agent --force --ignore-checksums
sc.exe config wazuh start=delayed-auto

$ossecConfig = 'C:\Program Files (x86)\ossec-agent\ossec.conf'
$xml = [xml] (gc $ossecConfig)
$child = $xml.CreateElement("hotfixes") #to enable windows vulnerabilities monitoring on agent
$child.InnerText = "yes"
$xml.ossec_config.wodle[0].AppendChild($child) | out-null
$xml.Save($ossecConfig)

$path = "C:\Program Files (x86)\ossec-agent\ossec.conf"; ((get-content -path $path -raw) -replace "0.0.0.0","${WazuhIP}") | set-content -path $path
$auth = "C:\Program Files (x86)\ossec-agent\agent-auth.exe"; .$auth -m ${WazuhIP}
Restart-Service wazuh