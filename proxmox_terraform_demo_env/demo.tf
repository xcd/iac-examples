resource "random_password" "demo_password" {
  length = 20
  special = true
}

data "template_cloudinit_config" "cloudbase_init_demo" {
  for_each = toset(var.demo_customers)

  gzip          = false
  base64_encode = false

  part {
    content_type = "text/cloud-config"
    filename = "cloud-config"
    content = <<-EOF
    set_hostname: demo-${each.value}
    users:
      -
        name: Administrator
        gecos: Demo-${each.value} admin
        primary_group: administrators
        passwd: ${random_password.demo_password.result}
        inactive: False
    runcmd: 
      - 'powershell -command ""'
      EOF
  }

  part {
    content_type = "text/x-shellscript"
    filename     = "install-software.ps1"
    content      = templatefile("${path.module}/files/scripts/install-software.ps1", {
      WazuhIP = var.wazuh_ip
      }
    )
  }

  part {
    content_type = "text/x-shellscript"
    filename     = "apply-system-tweaks.ps1"
    content      = file("${path.module}/files/scripts/apply-system-tweaks.ps1")
  }

  part {
    content_type = "text/x-shellscript"
    filename     = "join-domain.ps1"
    content      = templatefile("${path.module}/files/scripts/join-domain.ps1", {
      dc_admin_password = var.dc_admin_password
      }
    )
  }
}
 
resource "local_file" "cloudbase_init_demo" {
  for_each = toset(var.demo_customers)
  file_permission = "0700"
  content   = data.template_cloudinit_config.cloudbase_init_demo[each.value].rendered
  filename  = "${path.module}/ci_snippets/cloudbase_init_demo_${each.value}.cfg"
}

resource "null_resource" "cloudbase_init_demo" {
  for_each = toset(var.demo_customers)
  connection {
    type     = "ssh"
    port     = "54321"
    user     = "${var.terraform_user}"
    password = "${var.terraform_password}"
    host     = "${var.proxmox_ip}"
  }

  provisioner "file" {
    source       = local_file.cloudbase_init_demo[each.value].filename
    destination  = "/home/terraform/snippets/cloudbase_init_demo_${each.value}.yml"
  }
}

resource "proxmox_vm_qemu" "demo" {
  depends_on = [
    null_resource.cloudbase_init_demo
  ]

  for_each = toset(var.demo_customers)
  name = "demo-${each.value}"
  desc = "Terraform created VM"
  clone = "w19-template"
  full_clone = true
  target_node = "pve"
  oncreate = true

  os_type = "cloud-init"
  bios = "seabios"
  boot = "c"
  bootdisk = "scsi0"
  scsihw = "virtio-scsi-pci"
  hotplug = "0"

  agent = 1 
  cores = 1
  sockets = 2
  memory = 6144
  balloon = 6144

  ci_wait = 10
  cicustom = "user=tf_snippets:snippets/cloudbase_init_demo_${each.value}.yml"
  cloudinit_cdrom_storage = "local"

  vga {
    type = "virtio"
    memory = "64"
  }

  ipconfig0 = "ip=dhcp"

  network {
    model = "virtio"
    firewall = false
    bridge = "vmbr0"
  }

  disk { 
    size = "40G"
    type = "scsi"
    storage = "storage"
    ssd = 1 
    iothread = 1 
    cache = "none"
  }

  provisioner "file" {
    source = "${path.module}/files/deployment_toolkit"
    destination = "C:/deployment_toolkit"

    connection {
      type = "ssh"
      user = "Administrator"
      password = random_password.demo_password.result
      host = self.ssh_host
    }
  }

  lifecycle {
     ignore_changes = [
       network
     ]
  }
}
