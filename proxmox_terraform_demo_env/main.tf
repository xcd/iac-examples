terraform {
  required_version = ">= 0.15.0"
  required_providers {
    proxmox = {
      source = "Telmate/proxmox"
      version = "2.9.1"
    }
  }
}

provider "proxmox" {
	pm_api_url = "https://${var.proxmox_ip}:8006/api2/json"
  pm_tls_insecure = true 
}