variable "vm_name" {
  type        = string
  default     = "demo"
}

variable "demo_customers" {
  type        = list(string)
  default     = ["first", "second"]
}

variable "dc_admin_password" {
  type        = string 
  sensitive   = true
}

variable "wazuh_ip" {
  type = string
  sensitive = true 
}

variable "proxmox_ip" {
  type = string
  sensitive = true 
}

variable "terraform_user" {
  type        = string
  sensitive   = true
}

variable "terraform_password" {
  type        = string
  sensitive   = true
}
